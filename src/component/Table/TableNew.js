import React, { useEffect } from 'react';
// import Table from '@mui/material/Table';
// import TableBody from '@mui/material/TableBody';
// import TableCell from '@mui/material/TableCell';
// import TableContainer from '@mui/material/TableContainer';
// import TableHead from '@mui/material/TableHead';
// import TableRow from '@mui/material/TableRow';
// import Paper from '@mui/material/Paper';
import { Checkbox, Switch } from '@mui/material';
import InfoIcon from '@mui/icons-material/Info';
import FormControlLabel from '@mui/material/FormControlLabel';
// import { Padding } from '@mui/icons-material';
import '../Table/TableNew.css'

//New Table
import { Row, Col } from 'react-bootstrap';
import { Avatar, Typography } from '@material-ui/core';
import CheckCircleIcon from '@mui/icons-material/CheckCircle';
import CancelIcon from '@mui/icons-material/Cancel';

// const datas = [{
//     name: "Mohammed Salim",
//     status: "Present",
//     id: 2020171828,
//     buzzer1: <CancelIcon className='ml-5' sx={{ color: "red" }} />,
//     buzzer: <CheckCircleIcon className='ml-5' sx={{ color: "green" }} />,
//     Primary: <CheckCircleIcon className='ml-5' sx={{ color: "green" }} />
// },
// {
//     name: "Mohammed Salim",
//     status: "Leave",
//     id: 2020171828,
//     buzzer1: <CancelIcon className='ml-5' sx={{ color: "green" }} />,
//     buzzer: <CheckCircleIcon className='ml-5' sx={{ color: "red" }} />,
//     Primary: <CancelIcon className='ml-5' sx={{ color: "red" }} />
// },
// {
//     name: "Mohammed Salim",
//     status: "Absent",
//     id: 2020171828,
//     buzzer1: <CancelIcon className='ml-5' sx={{ color: "red" }} />,
//     buzzer: <CheckCircleIcon className='ml-5' sx={{ color: "green" }} />,
//     Primary: <CancelIcon className='ml-5' sx={{ color: "red" }} />
// },
// {
//     name: "Mohammed Salim",
//     status: "Present",
//     id: 2020171828,
//     buzzer1: <CancelIcon className='ml-5' sx={{ color: "green" }} />,
//     buzzer: <CheckCircleIcon className='ml-5' sx={{ color: "red" }} />,
//     Primary: <CancelIcon className='ml-5' sx={{ color: "red" }} />
// },
// {
//     name: "Mohammed Salim",
//     status: "Leave",
//     id: 2020171828,
//     buzzer1: <CancelIcon className='ml-5' sx={{ color: "red" }} />,
//     buzzer: <CheckCircleIcon className='ml-5' sx={{ color: "green" }} />,
//     Primary: <CancelIcon className='ml-5' sx={{ color: "red" }} />
// },
// {
//     name: "Mohammed Salim",
//     status: "Present",
//     id: 2020171828,
//     buzzer1: <CancelIcon className='ml-5' sx={{ color: "red" }} />,
//     buzzer: <CheckCircleIcon className='ml-5' sx={{ color: "green" }} />,
//     Primary: <CheckCircleIcon className='ml-5' sx={{ color: "green" }} />
// },
// {
//     name: "Mohammed Salim",
//     status: "Present",
//     id: 2020171828,
//     buzzer1: <CancelIcon className='ml-5' sx={{ color: "red" }} />,
//     buzzer: <CheckCircleIcon className='ml-5' sx={{ color: "green" }} />,
//     Primary: <CheckCircleIcon className='ml-5' sx={{ color: "green" }} />
// },
// {
//     name: "Mohammed Salim",
//     status: "Absent",
//     id: 2020171828,
//     buzzer1: <CancelIcon className='ml-5' sx={{ color: "red" }} />,
//     buzzer: <CheckCircleIcon className='ml-5' sx={{ color: "green" }} />,
//     Primary: <CheckCircleIcon className='ml-5' sx={{ color: "green" }} />
// },
// {
//     name: "Mohammed Salim",
//     status: "Present",
//     id: 2020171828,
//     buzzer1: <CancelIcon className='ml-5' sx={{ color: "red" }} />,
//     buzzer: <CheckCircleIcon className='ml-5' sx={{ color: "green" }} />,
//     Primary: <CheckCircleIcon className='ml-5' sx={{ color: "green" }} />
// },
// {
//     name: "Mohammed Salim",
//     status: "Present",
//     id: 2020171828,
//     buzzer1: <CancelIcon className='ml-5' sx={{ color: "red" }} />,
//     buzzer: <CheckCircleIcon className='ml-5' sx={{ color: "green" }} />,
//     Primary: <CheckCircleIcon className='ml-5' sx={{ color: "green" }} />
// }]


export default function NewTable(props) {

    const {
        // setData,
        setFilterDatas,
        // mainArray,
        filterdatas,
        // handleListSwitchChange,
        headerPrimary,
        handleHeaderPrimary,
        headerBuzzer,
        handleHeaderBuzzer,
        headerBuzzer1,
        handleHeaderBuzzer1,
        headerSwitch,
        value } = props


    let currentList;
    if (value === '1') {
        currentList = "Absent";
    }
    if (value === '2') {
        currentList = "Present";
    }
    const filteredData = filterdatas.filter(item =>
        item.availability === currentList
    )


    const headerSwitchValue = () => {
        let currentList = "Present";
        // if (value === '1') {
        //     currentList = "Present";
        // }
        // if (value === '2') {
        //     currentList = "Present";
        // }
        // console.log(filterdatas, "filterData")
        const filteredData = filterdatas.filter(item =>
            item.availability === currentList
        )
        //Header Switch works if i have currentList on Present. The above part is only for this function 
        return (filteredData.length === filterdatas.length)
    }
    let headerValue = headerSwitchValue()
    // console.log(headerValue)
    // const [listChecked, setListChecked] = useState(false);

    const handleSwitchColor = (data, dataIndex) => {
        if (headerPrimary && headerBuzzer && headerBuzzer1) {
            return (
                (data.Primary === "green" && data.buzzer === "green" && data.buzzer1 === "green") ? ("success") : ("warning")
            )
        }
        if (headerPrimary && headerBuzzer) {
            return (
                (data.Primary === "green" && data.buzzer === "green") ? ("success") : ("warning")
            )
        }
        if (headerBuzzer && headerBuzzer1) {
            return (
                (data.buzzer === "green" && data.buzzer1 === "green") ? ("success") : ("warning")
            )
        }
        if (headerPrimary && headerBuzzer1) {
            return (
                (data.Primary === "green" && data.buzzer1 === "green") ? ("success") : ("warning")
            )
        }
        if (headerPrimary) {
            return (
                (data.Primary === "green") ? ("success") : ("warning")
            )
        }
        if (headerBuzzer) {
            return (
                (data.buzzer === "green") ? ("success") : ("warning")
            )
        }
        if (headerBuzzer1) {
            return (
                (data.buzzer1 === "green") ? ("success") : ("warning")
            )
        }
    }

    useEffect(() => {
        // console.log({ filterdatas })
        const copyData = filterdatas.map((item, itemIndex) => {
            const data = { ...item }
            if (data.availability === "Absent" && handleSwitchColor(data, itemIndex) === "success")
                data.availability = "Present";
            return data
        })
        /* copyData.map((data, dataIndex) => {
            // console.log(data.availability === "Absent" && handleSwitchColor(data, dataIndex) === "success")
            if (data.availability === "Absent" && handleSwitchColor(data, dataIndex) === "success") {
                    // console.log(dataIndex === mdataIndex)
                        // console.log("inside true")
                        data.availability = "Present";
                        // mdata.onLoadAvailability = "Present"
                        // console.log(mdata)
                        // console.log(mdata)
                        console.log(data, "useEffect availability")
            }
        }) */
        // console.log(copyData, "copyData")
        setFilterDatas(copyData);
    }, [])
    // console.log(filterdatas, "filterdatas after setting present")



    // State for Check Value if the Condition Satisfies

    // const temp = () => {
    //     filteredData.map((data, dataIndex) => {
    //         // console.log(data, "filteredData")
    //         if (data.availability === "Present") {
    //             return true;
    //         } else{
    //             console.log("inside else")
    //             return false;
    //         }
    //     })
    // }
    // const [checked, setChecked] = useState(temp());
    // console.log(checked, "checked")
    // useEffect(() => {
    //     setChecked(temp())
    // }, [checked])



    // const absentSwitchPresnt = (data, dataIndex) => {
    //     console.log(data, "data", data.availability)
    //     console.log(handleSwitchColor(data, dataIndex))
    //     if (data.availability === "Present") {
    //         return true
    //     } else if (handleSwitchColor(data, dataIndex) === "success") {
    //         console.log("inside else if")
    //         const copyData = [...mainArray]
    //         return true
    //     } else {
    //         return false
    //     }
    // }

    // const uncheckSatisfiedSwitch = (data, dataIndex) => {
    //     if(value === '1'){
    //     if(data.availability === "Absent" && handleSwitchColor(data, dataIndex) === "success"){
    //         <Switch 
    //         color={handleSwitchColor(data, dataIndex)}
    //         checked={true}
    //         onChange={(event) => handleListSwitchChange(event, dataIndex, data.no)}
    //     />
    //     }else{
    //         <Switch 
    //         color={handleSwitchColor(data, dataIndex)}
    //         checked={false}
    //         onChange={(event) => handleListSwitchChange(event, dataIndex, data.no)}
    //     />
    //     }}else{
    //         <Switch 
    //         color={handleSwitchColor(data, dataIndex)}
    //         checked={data.availability === "Present"}
    //         onChange={(event) => handleListSwitchChange(event, dataIndex, data.no)}
    //     />
    //     }
    // }

    const handleListSwitchChange = (event, dataIndex, no) => {
        const copyData = [...filterdatas]
        const chengedFilterDatas = copyData.map((data, index) => {
            const changeCheckBoxObj = { ...data }
            if (changeCheckBoxObj.no === no) {
                (event.target.checked === false) ? (changeCheckBoxObj.availability = "Absent"
                ) : (
                    changeCheckBoxObj.availability = "Present")
            }
            return changeCheckBoxObj
        })
        setFilterDatas(chengedFilterDatas)
    };

    return (
        <>
            <Row className="w-100 ml-0 border  row-color ak " >
                <Col md={4} lg={4} className=" border-right d-flex align-items-center">
                    <InfoIcon color="action" fontSize="small" />
                    <div className='text-size'>FINAL ATTENDANCE REPORT WILL GENERATE BASED ON THE SELECTION</div>
                </Col>
                <Col md={2} col={2} className="border-right pl-4"  >
                    <FormControlLabel
                        value="end"
                        control={<Checkbox sx={{ color: "blue", '& .MuiSvgIcon-root': { fontSize: 15 } }}
                            checked={headerBuzzer1}
                            onClick={(event) => handleHeaderBuzzer1(event, value)}
                        />}
                        label="Buzzer(01)"
                        labelPlacement="end"
                        className=' mb-0 p-0'
                        size="small"
                    />
                    <Typography className='mb-0 ' variant='subtitle2'>Latest -10:45 AM</Typography>
                </Col>
                <Col md={2} col={2} className="border-right pl-5" >
                    <FormControlLabel
                        value="end"
                        control={<Checkbox sx={{ color: "blue", '& .MuiSvgIcon-root': { fontSize: 15 } }}
                            checked={headerBuzzer}
                            onClick={(event) => handleHeaderBuzzer(event, value)}
                        />}
                        label="Buzzer"
                        labelPlacement="end"
                        className='mb-0  p-0'
                        size="small"
                    />
                    <p className='mb-0'>10:15 AM</p>
                </Col>
                <Col md={2} col={2} className="border-right pl-5" >
                    <FormControlLabel
                        value="end"
                        control={
                            <Checkbox sx={{ color: "blue", '& .MuiSvgIcon-root': { fontSize: 15 } }}
                                checked={headerPrimary}
                                onClick={(event) => handleHeaderPrimary(event, value)}
                            />}
                        label="Primary"
                        labelPlacement="end"
                        className='mb-0  p-0'
                        size="small"
                    />
                    <p className='mb-0'>10:00 AM</p>
                </Col>
                <Col md={2} col={2} className="pl-5" >
                    <FormControlLabel
                        value="top"
                        control={<Switch
                            color="success"
                            checked={headerValue}
                            disabled={headerPrimary === false && headerBuzzer === false && headerBuzzer1 === false}
                            onChange={(event) => headerSwitch(event, value)} />}
                        label="Status"
                        labelPlacement="top"
                        className='ml-4'
                    />
                </Col>
            </Row>
            {filterdatas.map((data, dataIndex) => {
                return (
                    <Row className="w-100 ml-0 border row-width " key={dataIndex}>
                        <Col md={4} col={4} className="d-flex align-items-center border-right pt-1">
                            <div className='d-flex align-items-center'><Avatar alt="Remy Sharp" src="https://www.w3schools.com/howto/img_avatar.png" /></div>
                            <div className='ml-2'><p className='mb-0 '>{data.name}</p>
                                <p className='m-0 text-size1'>{data.id}</p></div>
                        </Col>
                        <Col md={2} col={2} className="border-right pt-2">
                            {(data.buzzer1 === "red") ? (<CancelIcon className='ml-5' sx={{ color: "red" }} />) : (<CheckCircleIcon className='ml-5' sx={{ color: "green" }} />)}
                        </Col>
                        <Col md={2} col={2} className="border-right pt-2">
                            {(data.buzzer === "red") ? (<CancelIcon className='ml-5' sx={{ color: "red" }} />) : (<CheckCircleIcon className='ml-5' sx={{ color: "green" }} />)}
                        </Col>
                        <Col md={2} col={2} className="border-right pt-2 ">
                            {(data.Primary === "red") ? (<CancelIcon className='ml-5' sx={{ color: "red" }} />) : (<CheckCircleIcon className='ml-5' sx={{ color: "green" }} />)}
                        </Col>
                        <Col md={2} col={2} className=" pl-5 pt-1">

                            <FormControlLabel
                                value="top"
                                control={
                                    <Switch
                                        color={handleSwitchColor(data, dataIndex)}
                                        checked={data.availability === "Present"}
                                        disabled={headerPrimary === false && headerBuzzer === false && headerBuzzer1 === false}
                                        onChange={(event) => handleListSwitchChange(event, dataIndex, data.no, data, filterdatas, currentList)}
                                    />
                                }
                                labelPlacement="top"
                                className='ml-4'
                            />
                        </Col>
                    </Row>
                )
            })}
        </>
    );
}