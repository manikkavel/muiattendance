import { TabContext, TabList, TabPanel } from '@material-ui/lab'
import { Button, Tab, Typography } from '@mui/material'
import { Box } from '@mui/system'
import React, { useState, useEffect } from 'react'
import NewTable from '../Table/TableNew'

const dataAr = [{
    no: 1,
    name: "Mohammed Salim",
    availability: "Present",
    onLoadAvailability: "Present",
    id: 2020171828,
    buzzer1: "red",
    buzzer: "green",
    Primary: "green"
},
{
    no: 2,
    name: "Jim",
    availability: "Leave",
    onLoadAvailability: "Leave",
    id: 2020171828,
    buzzer1: "green",
    buzzer: "red",
    Primary: "red"
},
{
    no: 3,
    name: "Han",
    availability: "Leave",
    onLoadAvailability: "Leave",
    id: 2020171828,
    buzzer1: "red",
    buzzer: "green",
    Primary: "red"
},
{
    no: 4,
    name: "Jimmy",
    availability: "Present",
    onLoadAvailability: "Present",
    id: 2020171828,
    buzzer1: "green",
    buzzer: "red",
    Primary: "red"
},
{
    no: 5,
    name: "Mohammed",
    availability: "Leave",
    onLoadAvailability: "Leave",
    id: 2020171828,
    buzzer1: "red",
    buzzer: "green",
    Primary: "red"
},
{
    no: 6,
    name: "John",
    availability: "Present",
    onLoadAvailability: "Present",
    id: 2020171828,
    buzzer1: "green",
    buzzer: "green",
    Primary: "green"
},
{
    no: 7,
    name: "Paul",
    availability: "Present",
    onLoadAvailability: "Present",
    id: 2020171828,
    buzzer1: "red",
    buzzer: "green",
    Primary: "red"
},
{
    no: 8,
    name: "Mohammed Salman",
    availability: "Absent",
    onLoadAvailability: "Absent",
    id: 2020171828,
    buzzer1: "red",
    buzzer: "green",
    Primary: "green"
},
{
    no: 9,
    name: "Mohammed",
    availability: "Present",
    onLoadAvailability: "Present",
    id: 2020171828,
    buzzer1: "red",
    buzzer: "red",
    Primary: "green"
},
{
    no: 10,
    name: "Salim",
    availability: "Present",
    onLoadAvailability: "Present",
    id: 2020171828,
    buzzer1: "red",
    buzzer: "green",
    Primary: "red"
}]

export const MainUI = () => {
    const [value, setValue] = useState('1');
    const [data, setData] = useState(dataAr);
    const [headerPrimary, setHeaderPrimary] = useState(true);
    const [headerBuzzer, setHeaderBuzzer] = useState(false);
    const [headerBuzzer1, setHeaderBuzzer1] = useState(false);

    useEffect(() => {
        const isAllHeaderCheckBoxDisabled = headerPrimary === false && headerBuzzer === false && headerBuzzer1 === false
        const anyOneHeaderCheckBoxClicked = headerPrimary === true || headerBuzzer === true || headerBuzzer1 === true
        const requiredTab = value === '1' ? absentData : presentData
        if (isAllHeaderCheckBoxDisabled) {
            requiredTab.map(item => item.availability = 'Absent')
        }
        if (anyOneHeaderCheckBoxClicked) {
            requiredTab.map(item => item.availability = 'Present')
        }
        setData([...data])
    }, [headerPrimary, headerBuzzer, headerBuzzer1])

    const getStatus = (status) => data.filter((data) => data.onLoadAvailability === status)

    let presentData = getStatus("Present");
    let absentData = getStatus("Absent");
    let leaveData = getStatus("Leave");
    const presentNo = presentData.length;
    const absentNo = absentData.length;
    const leaveNo = leaveData.length;
    const handleCancel = () => {
        const rollBackDataOnLoad = (item) => { item.availability = item.onLoadAvailability }
        data.map(rollBackDataOnLoad)
        setData([...data]);
    }
    const handleDone = () => {
        const updateUserChangedData = (item) => { item.onLoadAvailability = item.availability }
        data.map(updateUserChangedData)
        setData([...data]);
    }
    const handleTabChange = (event, newValue) => {
        setValue(newValue);
    };
    const handleListSwitchChange = (event, dataIndex, no) => {
        const isSwitchSelected = event.target.checked
        data.map((item, index) => {
            if (item.no === no) {
                if (isSwitchSelected) item.availability = "Present"; else item.availability = "Absent"
            }
            return ''
        })
        setData([...data])
    };
    const handleHeaderSwitch = (event, value) => {
        console.log(event.target.checked, value)
        const isSwitchEnabled = event.target.checked
        let selectedArray = [];
        let availability = isSwitchEnabled ? 'Present' : 'Absent';
        if (value === '1') { selectedArray = absentData };
        if (value === '2') { selectedArray = presentData };
        selectedArray.map(item => data.availability = availability)
        setData([...data]);
        return
    };
    const handleHeaderPrimary = (event) => {
        // console.log(event.target.checked)
        if (!event.target.checked) { setHeaderPrimary(false) } else { setHeaderPrimary(true) }
        return
    }

    const handleHeaderBuzzer = (event) => {
        console.log(event.target.checked)
        if (!event.target.checked) { setHeaderBuzzer(false) } else { setHeaderBuzzer(true) }
        return
    }

    const handleHeaderBuzzer1 = (event) => {
        console.log(event.target.checked)
        if (!event.target.checked) { setHeaderBuzzer1(false) } else { setHeaderBuzzer1(true) }
        return
    }
    return <>
        <div className='d-flex'>
            <div >
                <Typography component="div">
                    <Box sx={{ fontWeight: 'bold', m: 1, fontSize: 'large' }}>Attendence Report</Box>
                    <Box sx={{ fontWeight: 500, m: 1, fontSize: 'large' }}>Summary : {presentNo}/{data.length} Students Present</Box>
                </Typography>
            </div>
            <div className='flex-grow-1'></div>
            <div className=''>
                <Button sx={{ mr: 2 }} variant="outlined" onClick={handleCancel}>Close</Button>
                <Button variant="contained" onClick={handleDone}>Done</Button>
            </div>
        </div>
        <div>
            <Box sx={{ width: '100%', typography: 'body1' }}>
                <TabContext
                    value={value}>
                    <Box sx={{ borderBottom: 1, borderColor: 'divider', }}>
                        <TabList
                            indicatorColor="primary"
                            //  classes={classes.indicator}
                            onChange={handleTabChange} >
                            <Tab sx={{ pr: "30px" }}
                                textColor="primary"
                                label={`ABSENT(${absentNo})`} value="1" />
                            <Tab sx={{ pr: "30px" }} label={`PRESENT(${presentNo})`} value="2" />
                            <Tab sx={{ pr: "30px" }} label={`LEAVE(${leaveNo})`} value="3" />
                        </TabList>
                    </Box>
                    <TabPanel value="1">
                        <NewTable
                            filterdatas={absentData}
                            mainArray={data}
                            handleListSwitchChange={handleListSwitchChange}
                            setData={setData}
                            headerSwitch={handleHeaderSwitch}
                            value={value}
                            // absentData={absentData}
                            handleHeaderPrimary={handleHeaderPrimary}
                            headerPrimary={headerPrimary}
                            handleHeaderBuzzer={handleHeaderBuzzer}
                            headerBuzzer={headerBuzzer}
                            handleHeaderBuzzer1={handleHeaderBuzzer1}
                            headerBuzzer1={headerBuzzer1}
                        />
                    </TabPanel>
                    <TabPanel value="2">
                        <NewTable
                            filterdatas={presentData}
                            mainArray={data}
                            handleListSwitchChange={handleListSwitchChange}
                            setData={setData}
                            headerSwitch={handleHeaderSwitch}
                            value={value}
                            // presentData={presentData}
                            handleHeaderPrimary={handleHeaderPrimary}
                            headerPrimary={headerPrimary}
                            handleHeaderBuzzer={handleHeaderBuzzer}
                            headerBuzzer={headerBuzzer}
                            handleHeaderBuzzer1={handleHeaderBuzzer1}
                            headerBuzzer1={headerBuzzer1}
                        />
                    </TabPanel>
                    <TabPanel value="3">
                        <NewTable
                            filterdatas={leaveData}
                            mainArray={data}
                            // handleListSwitchChange={handleListSwitchChange}
                            setData={setData}
                            // headerSwitch={handleHeaderSwitch}
                            value={value}
                            // absentData={absentData}
                            handleHeaderPrimary={handleHeaderPrimary}
                            headerPrimary={headerPrimary}
                            handleHeaderBuzzer={handleHeaderBuzzer}
                            headerBuzzer={headerBuzzer}
                            handleHeaderBuzzer1={handleHeaderBuzzer1}
                            headerBuzzer1={headerBuzzer1}
                        />
                    </TabPanel>
                </TabContext>
            </Box>
        </div>
    </>
}