import { TabContext, TabList, TabPanel } from '@material-ui/lab'
import { Button, Tab, Typography } from '@mui/material'
import { Box } from '@mui/system'
// import CheckCircleIcon from '@mui/icons-material/CheckCircle';
// import CancelIcon from '@mui/icons-material/Cancel';
import React, { useState, useEffect } from 'react'
// import BasicTable from '../Table/Table'
import NewTable from '../Table/TableNew'

const dataAr = [{
    no: 1,
    name: "Mohammed Salim",
    availability: "Absent",
    onLoadAvailability: "Absent",
    id: 2020171828,
    buzzer1: "red",
    buzzer: "green",
    Primary: "green"
},
{
    no: 2,
    name: "Jim",
    availability: "Leave",
    onLoadAvailability: "Leave",
    id: 2020171828,
    buzzer1: "green",
    buzzer: "red",
    Primary: "red"
},
{
    no: 3,
    name: "Han",
    availability: "Present",
    onLoadAvailability: "Present",
    id: 2020171828,
    buzzer1: "red",
    buzzer: "green",
    Primary: "red"
},
{
    no: 4,
    name: "Jimmy",
    availability: "Present",
    onLoadAvailability: "Present",
    id: 2020171828,
    buzzer1: "green",
    buzzer: "red",
    Primary: "red"
},
{
    no: 5,
    name: "Mohammed",
    availability: "Leave",
    onLoadAvailability: "Leave",
    id: 2020171828,
    buzzer1: "red",
    buzzer: "green",
    Primary: "red"
},
{
    no: 6,
    name: "John",
    availability: "Present",
    onLoadAvailability: "Present",
    id: 2020171828,
    buzzer1: "green",
    buzzer: "green",
    Primary: "green"
},
{
    no: 7,
    name: "Paul",
    availability: "Absent",
    onLoadAvailability: "Absent",
    id: 2020171828,
    buzzer1: "red",
    buzzer: "green",
    Primary: "red"
},
{
    no: 8,
    name: "Mohammed Salman",
    availability: "Absent",
    onLoadAvailability: "Absent",
    id: 2020171828,
    buzzer1: "red",
    buzzer: "green",
    Primary: "green"
},
{
    no: 9,
    name: "Mohammed",
    availability: "Present",
    onLoadAvailability: "Present",
    id: 2020171828,
    buzzer1: "red",
    buzzer: "red",
    Primary: "green"
},
{
    no: 10,
    name: "Salim",
    availability: "Present",
    onLoadAvailability: "Present",
    id: 2020171828,
    buzzer1: "red",
    buzzer: "green",
    Primary: "red"
}]

export const MainUI = () => {

    const [value, setValue] = useState('1');
    const [datas, setData] = useState(dataAr);
    const [headerPrimary, setHeaderPrimary] = useState(true);
    const [headerBuzzer, setHeaderBuzzer] = useState(false);
    const [headerBuzzer1, setHeaderBuzzer1] = useState(false);

    useEffect(() => {
        const getStatus = (status) => {
            return (
                datas.filter((data) => {
                    return data.onLoadAvailability === status
                }))
        }
        setPresentData(getStatus("Present"))
        setAbsentData(getStatus("Absent"))
        setLeaveData(getStatus("Leave"))
    }, [datas])

    const getStatus = (status) => {
        return (
            datas.filter((data) => {
                return data.onLoadAvailability === status
            }))
    }
    let [presentData, setPresentData] = useState(getStatus("Present"));
    let [absentData, setAbsentData] = useState(getStatus("Absent"));
    let [leaveData, setLeaveData] = useState(getStatus("Leave"));
    const presentNo = presentData.length;
    const absentNo = absentData.length;
    const leaveNo = leaveData.length;

    const handleSwitchColor = (data, dataIndex) => {
        if (headerPrimary && headerBuzzer && headerBuzzer1) {
            return (
                (data.Primary === "green" && data.buzzer === "green" && data.buzzer1 === "green") ? ("success") : ("warning")
            )
        }
        if (headerPrimary && headerBuzzer) {
            return (
                (data.Primary === "green" && data.buzzer === "green") ? ("success") : ("warning")
            )
        }
        if (headerBuzzer && headerBuzzer1) {
            return (
                (data.buzzer === "green" && data.buzzer1 === "green") ? ("success") : ("warning")
            )
        }
        if (headerPrimary && headerBuzzer1) {
            return (
                (data.Primary === "green" && data.buzzer1 === "green") ? ("success") : ("warning")
            )
        }
        if (headerPrimary) {
            return (
                (data.Primary === "green") ? ("success") : ("warning")
            )
        }
        if (headerBuzzer) {
            return (
                (data.buzzer === "green") ? ("success") : ("warning")
            )
        }
        if (headerBuzzer1) {
            return (
                (data.buzzer1 === "green") ? ("success") : ("warning")
            )
        }
    }

    useEffect(() => {
        // const copyData = [...datas]
        if (headerPrimary === false && headerBuzzer === false && headerBuzzer1 === false) {
            const copyAbsentData = absentData.map((data, dataIndex) => {
                const absentData = { ...data }
                if (absentData.availability === "Present" || absentData.availability === "Absent") {
                    absentData.availability = "Absent"
                }
                return absentData
            })
            setAbsentData(copyAbsentData)
            const copyPresentData = presentData.map((data, dataIndex) => {
                const presentData = { ...data }
                if (presentData.availability === "Present") { presentData.availability = "Absent" }
                return presentData
            })
            setPresentData(copyPresentData)
        }
        else if (headerPrimary === true || headerBuzzer === true || headerBuzzer1 === true) {
            const copyAbsentData = absentData.map((data, dataIndex) => {
                // console.log(data)
                const absentData = { ...data }
                if (absentData.availability === "Absent" && handleSwitchColor(absentData, dataIndex) === "success") {
                    // console.log("inside else")
                    absentData.availability = "Present"
                } else if (absentData.availability === "Present" && handleSwitchColor(absentData, dataIndex) === "success") {
                    // console.log("inside other else")
                    absentData.availability = "Present"
                } else {
                    absentData.availability = "Absent"
                }
                return absentData
            })
            setAbsentData(copyAbsentData)
            const copyPresentData = presentData.map((data, dataIndex) => {
                const presentData = { ...data }
                if (presentData.availability !== "Present") { presentData.availability = "Present" }
                return presentData
            })
            setPresentData(copyPresentData)
        }
        // setData(copyData)
    }, [headerPrimary, headerBuzzer, headerBuzzer1])


    const handleHeaderSwitch = (event, value) => {
        // console.log(event.target.checked, value)
        // const copyData = [...datas];
        let currentListType;
        if (value === '1') { currentListType = "Absent" };
        if (value === '2') { currentListType = "Present" };

        // if (currentListType === "Absent") {
        //     const copyAbsentData = absentData.map((data, dataIndex) => {
        //         const absentData= {...data}
        //         if (absentData.availability === "Absent") { absentData.availability = "Present" }
        //         return absentData
        //     })
        //     setAbsentData(copyAbsentData)
        // } else {

        //    const copyPresentData =  presentData.map((data, dataIndex) => {
        //     const presentData = {...data}
        //         if (presentData.availability === "Absent") { presentData.availability = "Present" }
        //         return presentData
        //     })
        //     setPresentData(copyPresentData)
        // }

        //To change headerSwitch value if unChecked
        //If user unchecked changes in both present and absent
        if (event.target.checked === false) {
            if (currentListType === "Absent") {
                const copyAbsentData = absentData.map((data, dataIndex) => {
                    const absentData = { ...data }
                    if (absentData.availability === "Present" || absentData.availability === "Absent") { absentData.availability = "Absent" }
                    return absentData
                })
                setAbsentData(copyAbsentData)
            } else {

                const copyPresentData = presentData.map((data, dataIndex) => {
                    const presentData = { ...data };
                    if (presentData.availability === "Present") { presentData.availability = "Absent" }
                    return presentData
                })
                setPresentData(copyPresentData)
            }
        }
        //If user checks/turnon switch changes in both pages
        else {
            if (currentListType === "Absent") {
                const copyAbsentData = absentData.map((data, dataIndex) => {
                    const absentData = { ...data }
                    if (absentData.availability === "Present" || absentData.availability === "Absent") { absentData.availability = "Present" }
                    return absentData
                })
                setAbsentData(copyAbsentData)
            } else {

                const copyPresentData = presentData.map((data, dataIndex) => {
                    const presentData = { ...data };
                    if (presentData.availability === "Absent") { presentData.availability = "Present" }
                    return presentData
                })
                setPresentData(copyPresentData)
            }
        }
        // setData(copyData);
        return
    };


    // const handleDone = () => {
    //     let swapData
    //     if (value === '1') swapData = absentData
    //     if (value === '2') swapData = presentData
    //     const fullData = [...datas];
    //     const EditedData = fullData.map((data) => {
    //         swapData.map((item) => {
    //             if (item.no === data.no) {
    //                 const fullSingleData = { ...data }
    //                 const swapSingleData = { ...item }
    //                 console.log(fullSingleData, "singleData")
    //                 return fullSingleData.onLoadAvailability = swapSingleData.availability
    //             }
    //         })
    //         return
    //     })
    //     console.log(EditedData, "Done EditedData")
    //     // setData(copyData);
    // }

    const handleDone = () => {
        //changing availability value to onLoad in both absent and present
        const copyAbsentData = [...absentData];
        const onLoadAbsentData = copyAbsentData.map((item) => {
            const singleAbsent = { ...item };
            singleAbsent.onLoadAvailability = singleAbsent.availability
            return singleAbsent
        })
        setAbsentData([...onLoadAbsentData])

        const copyPresentData = [...presentData];
        const onLoadPresentData = copyPresentData.map((data) => {
            const singlePresent = { ...data }
            singlePresent.onLoadAvailability = singlePresent.availability
            return singlePresent
        })
        setPresentData([...onLoadPresentData])

        // if (value === '1') {
        //     const copyData = [...datas]
        //     const lastArray = []
        //     const newArray = copyData.map((obj) => {
        //         let copyObj = { ...obj }
        //         console.log(copyObj, "copyObj")
        //         const temp = onLoadAbsentData.filter((item) => {
        //             let absentDataObj = { ...item }
        //             console.log(absentDataObj, "absentDataObj")
        //             console.log(copyObj.no === absentDataObj.no, "condition")
        //             if (copyObj.no === absentDataObj.no) {
        //                 return lastArray.push(absentDataObj)
        //             } else {
        //                 return lastArray.push(copyObj)
        //             }
        //         })
        //         console.log(temp, "temp")
        //         return ''
        //     })
        //     console.log(lastArray, "lastArray")
        //     // setData(newArray)
        // }
        
        //merging all three tabs values and setting to  Main Array
        const mergeNewAbsentPresent = [...onLoadAbsentData, ...onLoadPresentData]
        const lastArr = [...mergeNewAbsentPresent, ...leaveData]
        setData([...lastArr])
    }
    // console.log(datas, "datas")
    // console.log(absentData, "absentData")
    // console.log(presentData, "presentData")
    // console.log(leaveData, "leaveData")

    const handleCancel = () => {
        const copyData = [...datas];
        copyData.map((data) => {
            return (
                data.availability = data.onLoadAvailability)
        })
        setData(copyData);
        return
    }

    const handleTabChange = (event, newValue) => {
        setValue(newValue);
        return
    };

    const handleHeaderPrimary = (event) => {
        // console.log(event.target.checked)
        if (!event.target.checked) { setHeaderPrimary(false) } else { setHeaderPrimary(true) }
        return
    }

    const handleHeaderBuzzer = (event) => {
        // console.log(event.target.checked)
        if (!event.target.checked) { setHeaderBuzzer(false) } else { setHeaderBuzzer(true) }
        return
    }

    const handleHeaderBuzzer1 = (event) => {
        // console.log(event.target.checked)
        if (!event.target.checked) { setHeaderBuzzer1(false) } else { setHeaderBuzzer1(true) }
        return
    }

    return (
        <>
            <div className='d-flex'>
                <div >
                    <Typography component="div">
                        <Box sx={{ fontWeight: 'bold', m: 1, fontSize: 'large' }}>Attendence Report</Box>
                        <Box sx={{ fontWeight: 500, m: 1, fontSize: 'large' }}>Summary : {presentNo}/{datas.length} Students Present</Box>
                    </Typography>
                </div>
                <div className='flex-grow-1'></div>
                <div className=''>
                    <Button sx={{ mr: 2 }} variant="outlined" onClick={handleCancel}>Close</Button>
                    <Button variant="contained" onClick={handleDone}>Done</Button>
                </div>
            </div>
            <div>
                <Box sx={{ width: '100%', typography: 'body1' }}>
                    <TabContext
                        value={value}>
                        <Box sx={{ borderBottom: 1, borderColor: 'divider', }}>
                            <TabList
                                indicatorColor="primary"
                                //  classes={classes.indicator}
                                onChange={handleTabChange} >
                                <Tab sx={{ pr: "30px" }}
                                    textColor="primary"
                                    label={`ABSENT(${absentNo})`} value="1" />
                                <Tab sx={{ pr: "30px" }} label={`PRESENT(${presentNo})`} value="2" />
                                <Tab sx={{ pr: "30px" }} label={`LEAVE(${leaveNo})`} value="3" />
                            </TabList>
                        </Box>
                        <TabPanel value="1">
                            <NewTable
                                setFilterDatas={setAbsentData}
                                filterdatas={absentData}
                                mainArray={datas}
                                // handleListSwitchChange={handleListSwitchChange}
                                setData={setData}
                                headerSwitch={handleHeaderSwitch}
                                value={value}
                                // absentData={absentData}
                                handleHeaderPrimary={handleHeaderPrimary}
                                headerPrimary={headerPrimary}
                                handleHeaderBuzzer={handleHeaderBuzzer}
                                headerBuzzer={headerBuzzer}
                                handleHeaderBuzzer1={handleHeaderBuzzer1}
                                headerBuzzer1={headerBuzzer1}
                            />
                        </TabPanel>
                        <TabPanel value="2">
                            <NewTable
                                filterdatas={presentData}
                                setFilterDatas={setPresentData}
                                mainArray={datas}
                                // handleListSwitchChange={handleListSwitchChange}
                                setData={setData}
                                headerSwitch={handleHeaderSwitch}
                                value={value}
                                // presentData={presentData}
                                handleHeaderPrimary={handleHeaderPrimary}
                                headerPrimary={headerPrimary}
                                handleHeaderBuzzer={handleHeaderBuzzer}
                                headerBuzzer={headerBuzzer}
                                handleHeaderBuzzer1={handleHeaderBuzzer1}
                                headerBuzzer1={headerBuzzer1}
                            />
                        </TabPanel>
                        <TabPanel value="3">
                            <NewTable
                                filterdatas={leaveData}
                                setFilterDatas={setLeaveData}
                                mainArray={datas}
                                handleListSwitchChange={() => { }}
                                setData={setData}
                                headerSwitch={() => { }}
                                value={value}
                                // absentData={absentData}
                                handleHeaderPrimary={handleHeaderPrimary}
                                headerPrimary={headerPrimary}
                                handleHeaderBuzzer={handleHeaderBuzzer}
                                headerBuzzer={headerBuzzer}
                                handleHeaderBuzzer1={handleHeaderBuzzer1}
                                headerBuzzer1={headerBuzzer1}
                            />
                        </TabPanel>
                    </TabContext>
                </Box>
            </div>
        </>
    )
}
